using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Vector3 offset = new Vector3(0f, 0f, -10f);
    public float smoothTime = 0.25f;
    private Vector3 velocity = Vector3.zero;

    public int offsetX;
    public int offsetXChecker;
    public int offsetY;
    public int offsetYChecker;

    [SerializeField] private Transform player;

    private void Update()
    {
        var playerX = player.position.x;
        var playerY = player.position.y;
        if (playerX > offsetXChecker || playerY > offsetYChecker)
        {

            Vector3 targetPosition = player.position + offset;
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);

            offsetYChecker += offsetY;
        }
    }

    /*[SerializeField] private Transform player;
    public int offsetX;
    public int offsetXChecker;
    public int offsetY;
    public int offsetYChecker;


    private void Update()
    {
        var playerX = player.position.x;
        var playerY = player.position.y;
        if (playerX > offsetXChecker || playerY > offsetYChecker)
        {
            transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);
            offsetXChecker += offsetX;
            offsetYChecker += offsetY;
        }
    }*/
}
